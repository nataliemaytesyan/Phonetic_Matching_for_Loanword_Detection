import jellyfish

def find_similar_words(words1, words2, similarity_threshold=0.85, exact_match_threshold=1.0):
    identical_words = []
    similar_words = []
    
    for word1 in words1:
        for word2 in words2:
            similarity_score = jellyfish.jaro_winkler_similarity(word1[1], word2[1])
            if word1 == word2 and similarity_score >= exact_match_threshold:
                identical_words.append((word1, word2, similarity_score))
            elif word1 != word2 and similarity_score >= similarity_threshold:
                similar_words.append((word1, word2, similarity_score))

    # Sort similar words based on similarity score (descending order)
    similar_words.sort(key=lambda x: x[2], reverse=True)

    return identical_words, similar_words

if __name__ == "__main__":
    ipa_text1 = input("Enter the first IPA text (tuples in the format ('x', 'y')): ") # phoneticize.russian.tuples.py and phoneticize.armenian.tuples.py will output in this format
    ipa_text2 = input("Enter the second IPA text (tuples in the format ('x', 'y')): ")

    # Parse the input tuples
    import ast
    words1 = ast.literal_eval(ipa_text1)
    words2 = ast.literal_eval(ipa_text2)

    similarity_threshold = 0.7  # Adjust this threshold value as needed (I recommend staying above 0.5)
    exact_match_threshold = 1.0  

    identical_words, similar_words = find_similar_words(words1, words2, similarity_threshold, exact_match_threshold)

    if identical_words:
        print("Identical words found:")
        for word1, word2, similarity_score in identical_words:
            print(f"{word1} - {word2} (Similarity Score: {similarity_score:.2f})")
    else:
        # print("No identical words found.")
        None

    if similar_words:
        print("\nSimilar words found (most similar to least):")
        for word1, word2, similarity_score in similar_words:
            print(f"{word1} - {word2} (Similarity Score: {similarity_score:.2f})")
    else:
        print("\nNo similar words found.")


    # Save the output to a text file
    with open("output.txt", "w") as file:
        if identical_words:
            file.write("Identical words found:\n")
            for word1, word2, similarity_score in identical_words:
                file.write(f"{word1} - {word2} (Similarity Score: {similarity_score:.2f})\n")
        else:
            file.write("No identical words found.\n")

        if similar_words:
            file.write("\nSimilar words found (most similar to least):\n")
            for word1, word2, similarity_score in similar_words:
                file.write(f"{word1} - {word2} (Similarity Score: {similarity_score:.2f})\n")
        else:
            file.write("\nNo similar words found.\n")

    print("Output saved to 'output.txt'.")
