#removes armenian puncuation from armenian texts

def remove_punctuation_armenian(text):
    # List of Armenian punctuation characters
    armenian_punctuation = ",«»։՞՜՛՚՞՛,.․՝:՜;՞—-՛՞"

    # Remove punctuation characters from the text
    cleaned_text = ''.join(char for char in text if char not in armenian_punctuation)

    return cleaned_text

def main():
    armenian_text = input("Enter an Armenian text: ")
    cleaned_text = remove_punctuation_armenian(armenian_text)

    print("Cleaned text:", cleaned_text)

if __name__ == "__main__":
    main()
