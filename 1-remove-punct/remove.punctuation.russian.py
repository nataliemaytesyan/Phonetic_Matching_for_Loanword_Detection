# removes russian punctuation from russian texts

def remove_punctuation_russian(text):
    # List of Russian punctuation characters
    russian_punctuation = [',','»','«', '.', '?', '!', '—', ':', ';', '"', "'", '|', '/', '~', '...', '(', ')', '[', ']', '{', '}']


    # Remove punctuation characters from the text
    cleaned_sentence = ''.join(char for char in text if char not in russian_punctuation)

    return cleaned_sentence

def main():
    russian_sentence = input("Enter a Russian text: ")
    cleaned_sentence = remove_punctuation_russian(russian_sentence)

    print("Cleaned text:", cleaned_sentence)

if __name__ == "__main__":
    main()
