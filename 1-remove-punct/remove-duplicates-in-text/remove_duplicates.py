# make all the words lowercase + remove duplicates

def remove_duplicates(text):
    # Convert the text to lowercase
    lower_text = text.lower()

    # Split the text into individual words
    words = lower_text.split()

    # Create a list to store unique words
    unique_words = []

    # Iterate through each word
    for word in words:
        # If the word is not already in the unique_words list, add it
        if word not in unique_words:
            unique_words.append(word)

    # Join the unique words back into a string
    result = ' '.join(unique_words)
    return result

def main():
    # Get input text from the user
    text = input("Enter the text: ")

    # Remove duplicates and convert to lowercase
    processed_text = remove_duplicates(text)

    # Print the result
    print("Processed text:")
    print(processed_text)

if __name__ == "__main__":
    main()
