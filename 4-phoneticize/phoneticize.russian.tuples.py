def phoneticize_russian(text):
    # Define mappings for Russian IPA phoneticization
    russian_ipa_map = {
        'б': 'b',
        'в': 'v',
        'г': 'ɡ',
        'д': 'd',
        'ж': 'ʒ',
        'з': 'z',
        'к': 'k',
        'л': 'l',
        'м': 'm',
        'н': 'n',
        'п': 'p',
        'р': 'r',
        'с': 's',
        'т': 't',
        'ф': 'f',
        'х': 'x',
        'ц': 't͡s',
        'ч': 't͡ɕ',
        'ш': 'ʃɑ',
        'щ': 'ʃɑ', #same for the purposes of comaparing to armenian
        'й': 'j',
        'а': 'ɑ',
        'е': 'ɛ',
        'ё': 'ɵ',
        'и': 'i',
        'о': 'o',
        'у': 'u',
        'ы': 'ɨ',
        'э': 'ɛ',
        'ю': 'ju',
        'я': 'ja',
        'ъ': '',   # Hard sign
        'ь': '',   # Soft sign
        'ѣ': 'je', # Historical Cyrillic letter "yat"
        'і': 'i',  # Historical Cyrillic letter "i"
        'ѵ': 'i',  # Historical Cyrillic letter "izhitsa"
        'ѳ': 'f',  # Historical Cyrillic letter "fita"
        'ѫ': 'ɔ̃',  # Historical Cyrillic letter "big yus"
        'ѧ': 'ja', # Historical Cyrillic letter "little yus"
        'ѱ': 'ps', # Historical Cyrillic letter "psi"
        'ѯ': 'ks', # Historical Cyrillic letter "ksi"
        'ѧ': 'ja', # Historical Cyrillic letter "little yus"
        'і́': 'i', # Historical Cyrillic letter "i" with stress
        'ѳ́': 'f', # Historical Cyrillic letter "fita" with stress
        'ѱ́': 'ps', # Historical Cyrillic letter "psi" with stress
        'ї': 'ji', # Short i
        'є': 'je', # Short e
    }
    # Phoneticize the input text
    phoneticized_list = []
    for char in text.split():
        phoneticized_word = ""
        for c in char:
            phoneticized_char = russian_ipa_map.get(c.lower(), c)
            phoneticized_word += phoneticized_char
        phoneticized_list.append((char, phoneticized_word))

    return phoneticized_list

if __name__ == "__main__":
    input_text = input("Enter Russian text: ")
    phoneticized_output = phoneticize_russian(input_text)
    
    # Print the list of tuples
    print("Phoneticized output (List of tuples):", phoneticized_output)
    
    # Print the phonetic representations back-to-back
    phonetic_output_russian_back_to_back = " ".join(phonetic for _, phonetic in phoneticized_output)
    print("Phoneticized output (IPA back-to-back):", phonetic_output_russian_back_to_back)

    output_russian_file = "phonetic_output_russian1.txt"
    with open(output_russian_file, "w", encoding="utf-8") as file:
        for word, phonetic in phoneticized_output:
            file.write(f"{word}: {phonetic}\n")

    print(f"Phoneticized output saved to '{output_russian_file}'.")