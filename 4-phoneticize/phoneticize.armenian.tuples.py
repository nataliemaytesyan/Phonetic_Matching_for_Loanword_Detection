def phoneticize_armenian(text):
    # Define mappings for Armenian IPA phoneticization
    armenian_ipa_map = {
        'բ': 'b',
        'պ': 'p',
        'թ': 't',
        'դ': 'd',
        'գ': 'ɡ',
        'ձ': 'd͡z',
        'զ': 'z',
        'ջ': 'd͡ʒ',
        'չ': 't͡ɕ',
        'ճ': 't͡ʃ',
        'կ': 'k',
        'ղ': 'ʁ',
        'ս': 's',
        'մ': 'm',
        'ն': 'n',
        'շ': 'ʃ',
        'խ': 'х', # actually χ
        'ծ': 'ts',
        'ժ': 'ʒ',
        'հ': 'h',
        'ռ': 'ɾ',
        'տ': 't',
        'ր': 'ɹ', #actually a ɹ, modified to campare to armenian
        'ց': 't͡s',
        'վ': 'v',
        'ե': 'ɛ',
        'ֆ': 'f',
        'ո': 'o',
        'ք': 'k',
        'ա': 'ɑ',
        'յ': 'j',# actually je
        'է': 'ɛ',
        'ե': 'ɛ', # jɛ
        'ու': 'u',
        'օ': 'ɔ',
        'եւ': 'jev',  # Example: 'yev'
        'ես': 'ɛs',   # Example: 'es'
        'եմ': 'ɛm',   # Example: 'em'
        'եք': 'ɛkʰ',  # Example: 'yekh'
        'էի': 'ɛj',   # Example: 'ey'
        'էս': 'ɛs',   # Example: 'es'
        'եք': 'ɛkʰ',  # Example: 'yekh'
        'ոչ': 'voʧ',  # Example: 'voch'
        'ուր': 'ur',  # Example: 'oor'
        'ոտք': 'votʃkʰ',  # Example: 'votchkh'
    }

    # Phoneticize the input text
    original_text = text.split()
    phoneticized_list = []
    phoneticized_text = ""

    for indi_word in original_text:
        i = 0
        while i < len(indi_word):

            char = indi_word[i]
            if i + 1 < len(indi_word):
                next_char = indi_word[i + 1]
            else:
                None

            if char == 'ո' and next_char == 'ւ':  # Check for two-character mapping (ու)
                phoneticized_text += 'u'
                i += 2  # Skip the next character
            
            if char == 'ե' and next_char == 'ւ':  # Check for two-character mapping (եւ)
                phoneticized_text += 'jev'
                i += 2  # Skip the next character

            elif char == 'ո' and i == 0:  # Check if 'ո' is at the beginning of the word
                phoneticized_text += armenian_ipa_map.get('ո-', 'vo')
                i += 1

            elif char == 'ե' and i == 0:  # Check if 'ե' is at the beginning of the word
                phoneticized_text += armenian_ipa_map.get('ե-', 'jɛ')
                i += 1
        
            else:
                phoneticized_char = armenian_ipa_map.get(char.lower(), char)
                phoneticized_text += phoneticized_char
                i += 1
        phoneticized_text += " "

    
    phoneticized_split = phoneticized_text.split()
    for index in range(len(original_text)):
        phoneticized_list.append((original_text[index], phoneticized_split[index]))

    return phoneticized_list

if __name__ == "__main__":
    input_text = input("Enter armenian text: ")
    phoneticized_output = phoneticize_armenian(input_text)
    
    # Print the list of tuples
    print("Phoneticized output (List of tuples):", phoneticized_output)
    
    # Print the phonetic representations back-to-back
    phonetic_output_armenian_back_to_back = " ".join(phonetic for _, phonetic in phoneticized_output)
    print("Phoneticized output (IPA back-to-back):", phonetic_output_armenian_back_to_back)

    output_armenian_file = "phonetic_output_armenian1.txt"
    with open(output_armenian_file, "w", encoding="utf-8") as file:
        for word, phonetic in phoneticized_output:
            file.write(f"{word}: {phonetic}\n")

    print(f"Phoneticized output saved to '{output_armenian_file}'.")














