def remove_common_endings(word):
    common_endings = [
    'ը', 'ու', 'ուր', 'ուրեր', 'որ', 'եր', 'ի',
    'ներ', 'եր',
    'ի', 'ու', 'ույն', 'ին', 'այդ', 'ենթա', 'յուր', 'ում', 'ումն',
    'ների', 'ներից', 'ներու', 'երի', 'երից', 'երու', 'ները', 'ներ',
    'ի', 'ու', 'ին', 'ինն', 'ին', 'ինէ', 'ներին'
    'ներէն', 'երէն',
    'ը', 'ու', 'ուն', 'ու', 'ին', 'ը', 
    'ներ', 'երը', 'եր',
    'ով', 'ու', 'ուվ', 'ուվից', 'ուվ', 'ուվու', 'ավոր',
    'ներով', 'երով',
    'ից', 'ում', 'ումն', 'ում',
    'ներով', 'երով', 
    'յական', 'ական', 'ութական', 'ութական', 'ություն', 'ության',
    'ս','նք', 'յիք', 'են', 'ն'
]

    # Remove the common endings if found at the end of the word
    for ending in common_endings:
        if word.endswith(ending):
            return word[:-len(ending)]

    return word

def stem_armenian_text(text):
    # Split the input text into individual words
    words = text.split()

    # Perform stemming by removing common endings for each word in the list
    stemmed_words = [remove_common_endings(word) for word in words]

    return " ".join(stemmed_words)

if __name__ == "__main__":
    # Get user input for the larger amount of Armenian text
    print("Enter Armenian text (press Enter on a new line to finish input):")
    armenian_text = ""
    while True:
        line = input()
        if not line:
            break
        armenian_text += line + "\n"

    # Perform stemming on the Armenian text
    stemmed_armenian_text = stem_armenian_text(armenian_text)

    # Display the results
    print("\nOriginal Armenian text:")
    print(armenian_text)

    print("\nStemmed Armenian text:")
    print(stemmed_armenian_text)
