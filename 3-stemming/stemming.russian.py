def remove_common_endings(word):
    common_endings = [
    '-а', '-я', '-о', '-е', '-ий', '-й', '-ь',
    '-ы', '-и',
    '-ы', '-и', '-а', '-я', '-ов', '-ев', '-ей', '-ий', '-и',
    '-ов', '-ев', '-ей', '-ий', '-и',
    '-у', '-е', '-у', '-ю', '-у', '-ю', '-ми', '-ами',
    '-ам', 
    '-у', '-у', '-ю', '-а', '-ю',
    '-ы', '-и', '-ы',
    '-ом', '-ой', '-ом', '-ой', '-ою', '-ой',
    '-ами',
    '-е', '-е', '-е', '-е',
    '-ах', '-ах',
    '-ый', '-ая', '-ое', '-ие',
    '-ю', '-ешь', '-ет',
    '-ем', '-ете', '-ют'
]
    # Remove the common endings if found at the end of the word
    for ending in common_endings:
        if word.endswith(ending):
            return word[:-len(ending)]

    return word

def stemm_russian_text(text):
    # Split the input text into individual words
    words = text.split()

    # Perform stemming by removing common endings for each word in the list
    stemmed_words = [remove_common_endings(word) for word in words]

    return " ".join(stemmed_words)

if __name__ == "__main__":
    # Get user input for the larger amount of Russian text
    print("Enter Russian text (press Enter on a new line to finish input):")
    russian_text = ""
    while True:
        line = input()
        if not line:
            break
        russian_text += line + "\n"

    # Perform stemming on the Russian text
    stemmed_russian_text = stemm_russian_text(russian_text)

    # Display the results
    print("\nOriginal Russian text:")
    print(russian_text)

    print("\nStemmed Russian text:")
    print(stemmed_russian_text)