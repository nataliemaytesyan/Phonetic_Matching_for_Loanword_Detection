# Loanword or Word-on-Loan?

Welcome to the search for loanwords in low-resource languages!


## Table of Contents


1. [Introduction](#introduction)
2. [Installation](#installation)
3. [Usage](#usage)
- [Examples](#examples)
4. [License](#license)
5. [Contact](#contact)
6. [Resources](#resources)


## Introduction


I started this journey by taking a deep dive into the details of Natural Language Processing (NLP), only to discover that the models were heavily catered to the English language. Many of the techniques used in NLP for English had a hard time being applied to foreign languages, especially ones with small data sets and libraries. Of course, you could begin to manually train the program to work on another language but once again, the resources are incredibly limited on anything outside of the 5 most used languages.

Unfortunately, in my limited time I was unable to create truly effective recreations of NLP in low resource languages. Instead I opted to create a model that can phonetically identify loanwords across languages.

What is a loanword?

A word adopted from a foreign language with little or no modification. (Oxford Definition)
Some of the only modifications are adding on word endings that make the word better fit into the adopting language. These endings make it hard to tell when loanwords are used and are why I added step 3 (stemming) into my process.

What can loanwords tell us?

When speakers of two languages interact for an extended period of time, observing and exchanging one another’s customs and ideas, they will begin using words from each other’s languages. Linguists refer to the language a word originates from as the source language. The language that acquires the new, foreign word is known as the borrowing language or adopting language.

Generally, loanwords enter a language for one of two reasons: need and prestige. When one speech community comes into contact with another, members are often exposed to new objects and ideas that may not have existed in their own culture, and so speakers need a name for these new concepts. Other times, the receiving language may already have a word for a particular concept or object, but nevertheless borrows a word because of the prestige of the source language. In English, many such borrowings—also known as luxury loans—come from French. (0)

Loanwords can tell us a lot about the relationship between two cultures and are even more interesting when adopted by low-resource languages.

## Installation

1. Make sure you have the following installed:

- Python 3 (https://www.python.org)
- pip (https://pip.pypa.io/en/stable/installation/)


If you're running debian:


```bash
sudo apt install python3
sudo apt install pip
```


If you're running a different distribution, refer to the above links.

2. Install needed libraries:


```bash
pip install jellyfish
pip install ast
```

## Usage

The code is separated into folders for each step:

Apply steps 1-4 for both languages and use step 5 to compare them


1. Remove duplicates and Uppercase letters + Remove punctuation
2. Remove stop words
3. Stemming
4. Phoneticize
5. Compare phoneticized texts

Step 1: There is an extra folder within this step that removes duplicates, it isn't required but I personally think it keeps things cleaner. The main part of this step removes any punctuation. The Armenian and Russian punctuation removal files use the punctuation written in the respective lanuage's keyboard and include most possible punctuations. Modify the list of punctuation as needed.

Step 2: Removing stop words. Stop words refer to words in a stop list which are filtered out before processing natural language data because they are insignificant (1). As someone who is fluent in the languages I chose to use, I was able to do this part manually (though lists exist online). This code will output a chunk of text which only has "significant" meaning. It's almost impossible to account for every variation of a stop word, so feel free to add onto that list if you run into a word that doesn't filter properly. This list is more of a work in progress.

Step 3: Stemming is the process of reducing inflected words to their word stem, base or root form —generally a written word form (2). This is useful in rooting out endings that are added onto words to make them better fit the adopting language. It wouldn't hurt to run the output through step 2 again in the case that some of the stop words were modified words (it is completely optional though).

Step 4: This step will phoneticize your language using the International Phonetic Alphabet (IPA). Now a fair disclaimer, pronunciation is subjective and changes through dialects. For simplicity's sake I used the dialects of each language I speak as well as merged the IPA maps for a few sounds that were "close enough". This allowed step 5 to do its job a lot better. There are 3 outputs for this step. A list of tuples (original word, phonetics), a chunk of text which is just the phonetics back to back, and a .txt which has the same tuples in a document.

Step 5: I decided against using the Levenshtein Distance model to calculate the similarity in the two strings. I rewrote my program to instead use the Jaro-Winkler distance metric (included in the jellyfish library). Jaro-Winkler similarity is a string similarity metric that measures the similarity between two strings by calculating the number of matching characters and transpositions. The similarity score ranges from 0 to 1, where 0 indicates no similarity, and 1 indicates an exact match. This allowed me to control my outputs much more. This section will take the tuples that step 4 created as input. This way the program can compare the phonetics but also still maintain the original spelling data. I would recommend keeping the threshold to 0.7 or higher for accurate results. 

Once you have your tuples outputted, you can take a look at which words it paired and see if they were used in the same context. It is possible that you have identified a new loanword!


### Examples

(a)The Armenian text I will be using for this example: Այս վառարանը գազ է օգտագործում։
(b)The Russian text I will be using for this example: В этой плите используется газ.
Translation: (This stove uses gas.)

Step 1:
Run (a) using remove.punctuation.armenian.py
Run (b) using remove.punctuation.russian.py

(a1) Output of step 1 (armenian): Cleaned text: Այս վառարանը գազ է օգտագործում
(b1) Output of step 1 (russian): Cleaned text: В этой плите используется газ

As you can see, the punctuation is gone.


Step 2:
Run (a1) using StopWordRemoval.armenian.py
Run (b1) using StopWordRemoval.russian.py

(a2) Output of step 2 (armenian): Cleaned text: վառարանը գազ օգտագործում
(b2) Output of step 2 (russian): Cleaned text: этой плите используется газ

Unnecessary words are gone.


Step 3:
Run (a2) using stemming.armenian.py
Run (b2) using stemming.russian.py

(a3) Output of step 3 (armenian): Cleaned text: վառարան գազ օգտագործ
(b3) Output of step 3 (russian): Cleaned text: этой плите используется газ

Important to note these stems are far from perfect as they do not use machine learning models and are simply automated.


Step 4:
Run (a3) using phoneticize.armenian.tuples.py
Run (b3) using phoneticize.russian.tuples.py

(a4) Output of step 4 (armenian):
Enter armenian text: վառարան գազ օգտագործ
Phoneticized output (List of tuples): [('վառարան', 'vɑɾɑɹɑn'), ('գազ', 'ɡɑz'), ('օգտագործ', 'ɔɡtɑɡoɹts')]
Phoneticized output (IPA back-to-back): vɑɾɑɹɑn ɡɑz ɔɡtɑɡoɹts
Phoneticized output saved to 'phonetic_output_armenian1.txt'.

(b4) Output of step 4 (russian):
Enter Russian text: этой плите используется газ
Phoneticized output (List of tuples): [('этой', 'ɛtoj'), ('плите', 'plitɛ'), ('используется', 'ispolzuɛtsja'), ('газ', 'ɡɑz')]
Phoneticized output (IPA back-to-back): ɛtoj plitɛ ispolzuɛtsja ɡɑz
Phoneticized output saved to 'phonetic_output_russian1.txt'.


Step 5:
Run (a4, Phoneticized output (List of tuples)) and (b4, Phoneticized output (List of tuples)) using jellyfish.compare.tuple.py

Output:
Enter the first IPA text (tuples in the format ('x', 'y')): ('վառարան', 'vɑɾɑɹɑn'), ('գազ', 'ɡɑz'), ('օգտագործ', 'ɔɡtɑɡoɹts')
Enter the second IPA text (tuples in the format ('x', 'y')): ('этой', 'ɛtoj'), ('плите', 'plitɛ'), ('используется', 'ispolzuɛtsja'), ('газ', 'ɡɑz')

Similar words found (most similar to least):
('գազ', 'ɡɑz') - ('газ', 'ɡɑz') (Similarity Score: 1.00)
Output saved to 'output.txt'.

For this run I set the similarity threshold to 0.7 or higher. There was only one match in these sentences (ɡɑz) which was higher than that.

## How to recreate results

Refer to <create_figures>

## Recreating Presentation Slides and Research Paper
#### Paper 

To build the paper first change directories into the `paper/` folder 

```bash
cd paper/
```

Next, you can make the paper with a simple set of commands:

```bash
pdflatex main.tex
bibtex paper 
pdflatex main.tex 
pdflatex main.tex
```

#### Presentation 

To build the presentation, change directories into the `presentation/` folder from your original repository 

```bash
cd presentation/
```

Next, you can make the presentation and clean your repository with the following commands:

```bash
latexmk -xelatex slides.tex
latexmk -c
```

## Contact
If you have any questions, suggestions, or feedback, feel free to reach out at [natmaytesyan@gmail.com].

Happy coding!


## Resources

(0) https://www.antidote.info/en/blog/reports/loanword-or-word-on-loan#:~:text=Generally%2C%20loanwords%20enter%20a%20language,two%20reasons%3A%20need%20and%20prestige.&text=Once%20again%2C%20when%20one%20speech,name%20for%20these%20new%20concepts.
(1) https://en.wikipedia.org/wiki/Stop_word
(2) https://en.wikipedia.org/wiki/Stemming




















